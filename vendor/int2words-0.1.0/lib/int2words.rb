#!/usr/bin/ruby
#
# Integer number to English word conversion for numbers as large as 999 vigintillion (10^63)
# 
# Adapted from some Python code attributed to: vegaseat 07dec2006
#
# - Added dashes to words as appropriate
#
# - Corrected vigintillion which is not 10^60 as noted in that code; 
#   actually, the original implementation was missing quindecillion so
#   values quattuordecillion were off a little...
#
# http://www.quadibloc.com/math/bignum.htm
#

class Numeric

  ONES = ["","one","two","three","four","five","six","seven","eight","nine"]

  TEENS = ["ten","eleven","twelve","thirteen", "fourteen", 
          "fifteen","sixteen","seventeen","eighteen","nineteen"]

  DECAS = ["","","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"]

  THOUSANDS = ["",
              "thousand","million", "billion", "trillion", 
              "quadrillion", "quintillion", "sextillion", "septillion",
              "octillion", "nonillion", "decillion", "undecillion", 
              "duodecillion", "tredecillion", "quattuordecillion", "quindecillion", 
              "sexdecillion", "septendecillion", "octodecillion", "novemdecillion", 
              "vigintillion"]

  def to_words(opt={})
    opt[:show_num] ||= false
    opt[:as_string] ||= false
    opt[:prepend] ||= nil
    opt[:append] ||= nil

    num = self.round
    
    return ["Too Big"] if num >= 10**64
    return ["Zero"] if num == 0

    #
    # break number into array of triplets, reversed
    #
    # 1_505_390_248_594_782 => [782, 594, 248, 390, 505, 1]
    #
    triples = []
    numstr = num.to_s.reverse
    while numstr.size > 0
      triples << numstr.slice!(0..2).reverse.to_i
    end

    words = []
    triples.each_with_index do |triplet, thousands_index|
      # each triplet 
      # starting with hundreds, 782
      # then thousands, 594
      # then millions, 284
      # etc.
      
      # thousands_index indexes into THOUSANDS, ie "", "thousands", "millions"...

      # numeric subparts, hundreds, tens and ones
      # 7 hundred
      # 8 tens
      # 2 ones
      ones = triplet % 10
      tens = (triplet % 100)/10
      hundreds = (triplet % 1000)/100

      if triplet == 0 # skip if 0 value for a 1000 component
        next
      else # get thousands component
        thousands = THOUSANDS[thousands_index] 
      end

      subwords = []

      # prepend words as the array of thousands is traversed from least to most significant
      if tens == 0
        subwords << ONES[ones].capitalize + " " + thousands
      elsif tens == 1
        subwords << TEENS[ones].capitalize + " " + thousands 
      elsif tens > 1
        subwords << DECAS[tens].capitalize + (ones==0? ONES[ones].capitalize : "-"+ONES[ones] )  + " " + thousands
      end

      if hundreds > 0
        subwords << ONES[hundreds].capitalize + " " + "hundred " 
      end

      words << subwords.reverse.join
    end

    words[0].strip! # extraneous space for lsd

    words.reverse!

    words.unshift num.to_commas if opt[:show_num]
    words.unshift opt[:prepend] if opt[:prepend]
    words << opt[:append] if opt[:append]

    return opt[:as_string] ? words.join(" ") : words
  end

  def to_commas
    whole, frac = self.to_s.split(".")
    frac += "0" if frac.size == 1 unless frac.nil?
    c = { :value => "", :length => 0 }                                                
    r = whole.to_s.reverse.split("").inject(c) do |t, e|                              
      iv, il = t[:value], t[:length]                                                  
      iv += ',' if il % 3 == 0 && il != 0                                             
      { :value => iv + e, :length => il + 1 }                                         
    end                                                                               
    if frac.nil?
      r[:value].reverse!
    else
      r[:value].reverse! + "." + frac
    end
  end

end


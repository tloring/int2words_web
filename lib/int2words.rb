APP_ROOT = File.expand_path(File.join(File.dirname(__FILE__), '..'))

require 'rubygems'
$:.unshift File.join(APP_ROOT, 'vendor', 'sinatra', 'lib')
$:.unshift File.join(APP_ROOT, 'vendor', 'int2words-0.1.0', 'lib')
require 'sinatra'
require 'haml'
require 'int2words'

class Int2word < Sinatra::Application
  
  set :root, APP_ROOT  

  get '/style.css' do
    sass :style
  end

  get '/*' do 
    @value = params[:value]
    if params[:value]
      if params[:value] =~ /[-\/]/
        begin
          @date = params[:value]
          from_date = DateTime.parse(@date)
          from_sec =  Time.local(from_date.year, from_date.month, from_date.day, from_date.hour, from_date.min, from_date.sec)
          @num = (Time.now - from_sec).to_i
          @result = (Time.now - from_sec).to_i.to_words :show_num => true, :append => "<i>seconds since #{from_date.strftime('%A, %B %d, %Y %I:%M:%S %p')}</i>"
        rescue StandardError => e
          @error = e
        end
      else
        @result = params[:value].to_i.to_words :show_num => true
      end
    else
      num_str = params[:splat].first.gsub(/,/,'')
      num = num_str.empty? ? rand(10**rand(64)) : num_str.to_i
      @num = num
      @result = num.to_i.to_words :show_num => true
    end
    haml :index
  end

end

